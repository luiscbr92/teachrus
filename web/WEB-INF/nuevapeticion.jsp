<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
  <jsp:param name="menu" value="necesitas"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h1 class="page-header">Nueva petici�n</h1>

  <form action="" method="POST">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="col-md-7">
          <div class="form-group">
            <label for="Asignatura">Asignatura</label>
            <input type="text" class="form-control" name="asignatura" placeholder="Asignatura" maxlength="50" required />
          </div>
        </div>
        <div class="col-md-3 col-md-offset-2">
          <div class="form-group">
            <label for="creditos">Cr�ditos a pagar</label>
            <div class="input-group">
                <input type="text" class="form-control" name="creditos" pattern="[1-9][0-9]+" maxlength="2" required />
              <div class="input-group-addon"><span style="font-size: 15px;" class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
            <textarea class="form-control" rows="10" name="desc" placeholder="Descripci�n de lo que necesitas" maxlength="700" required></textarea>
          <br/>
        </div>
        
        

        <div class="col-md-12">
          <% if (request.getAttribute("error") != null) { %>
          <p class="text-danger"><%= request.getAttribute("error")%></p>
        <% }%>
          <button type="submit" class="btn btn-primary">Crear nueva peticion</button>
        </div>
        

      </div>
    </div>
  </form>

</div><!-- contenido del col de la derecha -->

<jsp:include page="footer.jsp"/>