<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
    <jsp:param name="menu" value="historial"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Historial de peticiones completadas</h1>

    <%
      ArrayList<Peticion> listaPeticiones = (ArrayList<Peticion>) request.getAttribute("listaPeticiones");
      for (Peticion p : listaPeticiones) {

    %>

    <div class="panel panel-default text-center">
        <div class="panel-body">
            <div class="col-md-1" >
                <span style="font-size: 60px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
            </div>
            <div class="col-md-6">
                <h3><%= p.getSolicitante().getNombre()%> <%= p.getSolicitante().getApellidos()%></h3>
            </div>


            <div class="col-md-2">
                <a class="btn btn-lg btn-info" href="/TeachRUs/detalle/<%= p.getId()%>" role="button">
                    Ver m�s
                </a>
            </div>
            <div class="col-md-2">
                <% if (p.getPuntuacion() != null && p.getPuntuacion() != 0) {%>

                <div class="row">
                    <div class="col-md-12 center-block">
                        <span class="glyphicon glyphicon-heart"></span>
                    </div>
                    <div class="col-md-12 center-block">
                        <%= p.getPuntuacion()%>
                    </div>
                </div>

                <% } else {%>     
                <button class="btn btn-lg btn-block btn-success">
                    Puntuar
                </button>

                <% }
                %>
            </div>
            <div class="col-md-1">
                <div class="row">
                    <div class="col-md-12 center-block">
                        <span class="glyphicon glyphicon-bitcoin"></span>
                    </div>
                    <div class="col-md-12 center-block">
                        <%= p.getCoste()%>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p><%= p.getInfo()%></p>
            </div>

            
        </div>
    </div>

    <%      }

    %>


</div><!-- contenido del col de la derecha -->

<jsp:include page="footer.jsp"/>