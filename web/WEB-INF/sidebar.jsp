<%@page import="business.Usuario"%>
<%@page import="data.UsuarioDB"%>
<div class="col-sm-3 col-md-2 sidebar">
  <div class="row text-center">
    <div class="col-md-5">
      <a href="/TeachRUs/verperfil/<%= ((Usuario) session.getAttribute("usuario")).getNick()%>">
        <span style="font-size: 80px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
      </a>
    </div>
    <div class="col-md-7">
      <div class="row">
        <div class="col-md-8 text-right"><h4><%= ((Usuario) session.getAttribute("usuario")).getCreditos()%></h4></div>
        <div class="col-md-4 text-left"><span style="font-size: 30px;" class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span></div>
        <div class="col-md-8 text-right"><h4><%= UsuarioDB.getReputacion(((Usuario) session.getAttribute("usuario")))%></h4></div>
        <div class="col-md-4 text-left"><span style="font-size: 30px;" class="glyphicon glyphicon-heart" aria-hidden="true"></span></div>
      </div>
    </div>
  </div>

  <div class="row" style="margin-top:50px">
    <ul class="tag_list">
      <li><a href="" class="tag rank1">software</a></li>
      <li><a href="" class="tag rank2">statistics</a></li>
      <li><a href="" class="tag rank3">teaching</a></li>
      <li><a href="" class="tag rank4">technology</a></li>
      <li><a href="" class="tag rank5">tipo</a></li>
      <li><a href="" class="tag rank6">ubuntu</a></li>
      <li><a href="" class="tag rank7">wordpress</a></li>
      <li><a href="" class="tag rank1">tutorials</a></li>
      <li><a href="" class="tag rank2">usability</a></li>
      <li><a href="" class="tag rank3">bool</a></li>
      <li><a href="" class="tag rank4">video</a></li>
      <li><a href="" class="tag rank5">work</a></li>
      <li><a href="" class="tag rank6">wiki</a></li>
      <li><a href="" class="tag rank7">web 2.0</a></li>
      <li><a href="" class="tag rank6">twitter</a></li>
      <li><a href="" class="tag rank7">php</a></li>
      <li><a href="" class="tag rank1">servlets</a></li>
      <li><a href="" class="tag rank2">xubuntu</a></li>
      <li><a href="" class="tag rank3">java</a></li>
    </ul>
  </div>
</div>