<jsp:include page="headerl.jsp"/>

<div id="box" class="container">

  <form class="form-signin" action="" method="POST">
    <h2 class="form-signin-heading">Entrar</h2>

    <input type="text" name="inputUsername" class="form-control" placeholder="Usuario" maxlength="20" required autofocus>

    <input type="password" name="inputPassword" class="form-control" placeholder="Contrase�a" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
    
    <% if (request.getAttribute("error") != null) { %>
      <p class="text-danger"><%= request.getAttribute("error") %></p>
    <% } %>
    
    
  </form>
  <label id="no-registrado">�A�n no te has registrado?</label>
  <a href="register" class="register_form btn_register btn btn-lg btn-primary btn-block">Registrarse</a>

</div> <!-- /container -->

<jsp:include page="footerl.jsp"/>