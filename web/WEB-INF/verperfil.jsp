<%@page import="business.Usuario"%>
<jsp:include page="header.jsp">
  <jsp:param name="menu" value=""/>
</jsp:include>
<jsp:include page="sidebar.jsp"/>
<%
  Usuario usuarioSolicitado = (Usuario) request.getAttribute("usuarioSolicitado");
  Integer ayudasOfrecidas = (Integer) request.getAttribute("ayudasOfrecidas");
  Integer ayudasRecibidas = (Integer) request.getAttribute("ayudasRecibidas");
  Integer diasRegistrado = (Integer) request.getAttribute("diasRegistrado");
  Integer puntuacionesRecibidas = (Integer) request.getAttribute("puntuacionesRecibidas");
  Usuario uSession = (Usuario) (request.getSession(false).getAttribute("usuario"));
%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="row">
    <div class="col-md-3 col-md-offset-3">
      <span style="font-size: 120px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
    </div>
    <div class="col-md-3">
      <h3><%=usuarioSolicitado.getNick()%></h3>

      <span style="font-size: 40px;" class="glyphicon glyphicon-heart" aria-hidden="true"></span>
      <p style="margin-left: 10px"><%= request.getAttribute("reputacion")%></p>

    </div>

  </div><!-- row-->



  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      <h4>Datos personales</h4>
    </div>
    <div class="col-md-4 col-md-offset-2" >


      <div class="col-md-12">Nombre</div>
      <div class="col-md-12">Apellidos</div>
      <div class="col-md-12">Ciudad</div>
      <div class="col-md-12">Carrera</div>
    </div>
    <div class="col-md-4" >

      <div class="col-md-12"><%=usuarioSolicitado.getNombre()%></div>
      <div class="col-md-12"><%=usuarioSolicitado.getApellidos()%></div>
      <div class="col-md-12"><%=usuarioSolicitado.getCiudad()%></div>
      <div class="col-md-12"><%=usuarioSolicitado.getCarrera()%></div>
    </div>
  </div><!--row-->
  <div class="row">
    <div class="col-md-10 col-md-offset-2">
      <h4>Estad�sticas</h4>

    </div>
    <div class="col-md-4 col-md-offset-2">
      <div class="col-md-12">Ayudas ofrecidas</div>
      <div class="col-md-12">Ayudas recibidas</div>
      <div class="col-md-12">Tiempo registrado</div>
      <div class="col-md-12">Votaciones de reputaci�n</div>
    </div>
    <div class="col-md-4">

      <div class="col-md-12"><%=ayudasOfrecidas%></div>
      <div class="col-md-12"><%=ayudasRecibidas%></div>
      <div class="col-md-12"><%=diasRegistrado%> D�as</div>
      <div class="col-md-12"><%=puntuacionesRecibidas%></div>

    </div>
  </div><!-- row -->

  <% if (!uSession.getNick().equals(usuarioSolicitado.getNick())) {%>
  <div class="row">
    <div class="col-md-3 col-md-offset-3">
      <br />
      <a class="mybutton btn btn-lg btn-primary btn-block" href="/TeachRUs/Mensaje/<%= usuarioSolicitado.getNick()%>">Enviar mensaje privado</a>
    </div>
  </div>
  <% }%>
</div>
<jsp:include page="footer.jsp"/>