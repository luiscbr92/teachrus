<%@page import="business.Respuesta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
    <jsp:param name="menu" value=""/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<%
  Peticion p = (Peticion) request.getAttribute("peticion");
  ArrayList<Respuesta> listaRespuestas = (ArrayList<Respuesta>) request.getAttribute("listaRespuestas");
  Usuario uSession = (Usuario) (request.getSession(false).getAttribute("usuario"));
%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="panel panel-default text-center">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <a href="/TeachRUs/verperfil/<%= p.getSolicitante().getNick()%>">
                        <span style="font-size: 60px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <p class="summary"><%= p.getSolicitante().getNombre()%> <%= p.getSolicitante().getApellidos()%></p>
                    </a>
                </div>
                <div class="col-md-8">
                    <h2>Carrera: <%= p.getSolicitante().getCarrera()%></h2>
                    <h2>Asignatura: <%= p.getAsignatura()%></h2>
                    <p class="summary"><%= p.getInfo()%></p>
                </div>
                <div class="col-md-2">
                    <span style="font-size: 20px;" class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span><h3><%= p.getCoste()%></h3>
                </div>
            </div>
            <%
              if (listaRespuestas.size() > 0) {
            %>
            <div class="col-md-12 collapse mybutton" id="collapse-respuestas">
                <%
                  for (int indiceRespuesta = 0; indiceRespuesta < listaRespuestas.size(); indiceRespuesta++) {
                    Respuesta r = listaRespuestas.get(indiceRespuesta);
                %>
                <div class="row well well-sm">
                    <div class="col-md-2">
                        <a href="/TeachRUs/verperfil/<%= r.getAutor().getNick()%>">
                            <span style="font-size: 40px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <p class="summary"><%= r.getAutor().getNombre()%> <%= r.getAutor().getApellidos()%></p>
                        </a>
                    </div>
                    <div class="col-md-7 col-md-offset-1">
                        <p><%=r.getTexto()%></p>
                    </div>

                    <%
                      if (p.getSolicitante().esIgual(uSession) && p.getRespuestaAceptada() == null) {
                    %>
                    <div class="col-md-2">
                        <form action="/TeachRUs/AceptarRespuesta" method="POST">
                            <input type="hidden" name="peticion" value="<%= p.getId()%>" />
                            <input type="hidden" name="respuesta" value="<%= r.getId()%>" />
                            <button type="submit" class="btn btn-block btn-success">
                                Aceptar
                            </button>
                        </form>
                    </div>
                    <% }
                      if (p.getRespuestaAceptada() != null && r.esIgual(p.getRespuestaAceptada())) {
                    %>
                    <div class="col-md-2">
                        <span class="glyphicon glyphicon-ok" style="font-size: 40px"></span>
                    </div>

                    <% }
                    %>
                </div>
                <% }
                %>

            </div>
            <div class="col-md-12 col-sm-12">
                <button  class="btn btn-warning btn-block mybutton" type="button" data-toggle="collapse" href="#collapse-respuestas"
                         aria-expanded="true" aria-controls="collapse-respuestas" onclick="
                                 $(this).find('span').toggleClass('glyphicon-chevron-up');
                                 $(this).find('span').toggleClass('glyphicon-chevron-down');
                         ">
                    <span id="flecha1" class="glyphicon glyphicon-chevron-down" ></span>
                    Ver todas las respuestas
                    <span id="flecha2" class="glyphicon glyphicon-chevron-down" ></span>
                </button>
            </div>

            <% }
            %>
        </div>
    </div>
    <%
      if (!p.getSolicitante().esIgual(uSession) && p.getRespuestaAceptada() == null) {
    %>
    <hr>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <button class="btn btn-lg btn-primary btn-block" type="button" data-toggle="collapse" href="#nueva-respuesta"
                    aria-expanded="false" aria-controls="nueva-respuesta">
                Escribir una respuesta
            </button>
        </div>
    </div>
    <div class="row collapse mybutton" id="nueva-respuesta">
        <form action="" method="POST">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="page-header">Nueva respuesta</h2>
                        <textarea class="form-control" rows="5" name="texto" placeholder="Escribe aqu� la respuesta de esta petici�n..." maxlength="700" required></textarea>
                        <br/>
                        <button type="submit" class="btn btn-primary col-md-6 col-md-offset-3">Enviar respuesta</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <% }
    %>
</div>

<jsp:include page="footer.jsp"/>