<%@page import="business.Respuesta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
    <jsp:param name="menu" value="necesitas"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Necesitas...</h1>
    <a class="btn btn-lg btn-primary" href="nuevapeticion" role="button">
        Nueva
    </a>

    <hr>

    <%
      ArrayList<Peticion> listaPeticiones = (ArrayList<Peticion>) request.getAttribute("listaPeticiones");
      ArrayList<ArrayList<Respuesta>> todasRespuestas = (ArrayList<ArrayList<Respuesta>>) request.getAttribute("todasRespuestas");
      for (int indicePeticion = 0; indicePeticion < listaPeticiones.size(); indicePeticion++) {
        Peticion p = listaPeticiones.get(indicePeticion);
        ArrayList<Respuesta> listaRespuestas = todasRespuestas.get(indicePeticion);

    %>

    <div class="panel panel-default text-center">
        <div class="panel-body">
            <div class="col-md-1">
                <span class="badge" style="font-size: 24px; background-color: orange;"> <%= listaRespuestas.size()%> </span>
            </div>
            <div class="col-md-8">
                <p class="summary"><%= p.getInfo()%></p>
            </div>
            <div class="col-md-3">
                <a class="btn btn-block btn-info" href="detalle/<%= p.getId()%>" role="button">
                    Ver m�s
                </a>
            </div>

            <%
              if (listaRespuestas.size() > 0) {
            %>
            <div class="col-md-12 collapse mybutton" id="<%=p.getId()%>">
                <%
                  for (int indiceRespuesta = 0; indiceRespuesta < listaRespuestas.size(); indiceRespuesta++) {
                    Respuesta r = listaRespuestas.get(indiceRespuesta);
                %>
                <div class="row well well-sm">
                    <div class="col-md-2">
                        <a href="/TeachRUs/verperfil/<%= r.getAutor().getNick()%>">
                            <span style="font-size: 40px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <p class="summary"><%= r.getAutor().getNombre()%> <%= r.getAutor().getApellidos()%></p>
                        </a>
                    </div>
                    <div class="col-md-7">
                        <p><%=r.getTexto()%></p>
                    </div>
                    <div class="col-md-3">

                        <form action="/TeachRUs/AceptarRespuesta" method="POST">
                            <input type="hidden" name="peticion" value="<%= p.getId() %>" />
                            <input type="hidden" name="respuesta" value="<%= r.getId() %>" />
                            <button type="submit" class="btn btn-block btn-success">
                                Aceptar este postulante
                            </button>
                        </form>
                    </div>
                </div>
                <% }
                %>

            </div>
            <div class="col-md-12 col-sm-12">
                <button class="btn btn-warning btn-block mybutton" type="button" data-toggle="collapse" href="#<%= p.getId()%>"
                        aria-expanded="true" aria-controls="<%=p.getId()%>"  onclick="
                                $(this).find('span').toggleClass('glyphicon-chevron-up');
                                $(this).find('span').toggleClass('glyphicon-chevron-down');
                        ">
                    <span id="flecha1" class="glyphicon glyphicon-chevron-down" ></span>
                    Ver postulantes
                    <span id="flecha2" class="glyphicon glyphicon-chevron-down" ></span>
                </button>
            </div>

            <% }
            %>
        </div>
    </div>

    <% }
    %>


</div><!-- contenido del col de la derecha -->

<jsp:include page="footer.jsp"/>