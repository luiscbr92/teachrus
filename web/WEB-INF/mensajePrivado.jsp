
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="business.Usuario"%>
<%@page import="business.Mensaje"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="header.jsp">
  <jsp:param name="menu" value=""/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h1 class="page-header">Mensajes</h1>
  <div class="col-md-offset-6">
  <form action="" method="post">
        <textarea class="form-control" rows="3" name="mens" placeholder="Escribe tu mensaje aquí..." maxlength="700"required></textarea>
        <button type="submit" class="btn btn-primary col-md-offset-9 mybutton">Enviar mensaje </button>
  </form>
</div>
  <br></br>
  <%
    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    ArrayList<Mensaje> conversacionMensajes = (ArrayList<Mensaje>) request.getAttribute("conversacionMensajes");
    if(conversacionMensajes!=null)
        for(Mensaje msj: conversacionMensajes ){
            if(msj.getEmisor().esIgual((Usuario)request.getSession().getAttribute("usuario"))){ 

    %>
  
    <div class="panel panel-default col-md-offset-4 col-md-8">
        <div class="panel-body">
            <div class="row row well-sm ">
                <div class="col-md-2 col-offset-10">
                        <a href="/TeachRUs/verperfil/<%=msj.getEmisor().getNick()%>">
                            <span class="glyphicon glyphicon-user" style="font-size:20px;"aria-hidden="true"></span>
                            <%=msj.getEmisor().getNick()%>
                        </a>
                    </span>
                </div>
                <div class="col-md-2">
                    <%=msj.getHoraEnvio().format(miFormato)%>
                </div>
                <div class="col-offset-4 col-md-8">
                    <p><%=msj.getTexto()%></p>
                </div>
            </div>
        </div>
    </div>
     <%
     }else{
    %>
    
    <div class="panel panel-default col-md-8">
        <div class="panel-body ">
            <div class="row row well-sm ">
                <div class="col-md-2 col-offset-8">                    
                    <a href="/TeachRUs/verperfil/<%=msj.getEmisor().getNick()%>">
                        <span class="glyphicon glyphicon-user" style="font-size:20px;"aria-hidden="true"></span>
                        <%=msj.getEmisor().getNick()%>
                    </a>
                </div>
                <div class="col-md-2">
                    <%=msj.getHoraEnvio().format(miFormato)%>
                </div>
              
                <div class="col-md-8">
                    <p><%=msj.getTexto()%></p>
                </div>                       
            </div>
        </div>
    </div>    
    <%
        }
    }
    %>
</div>


<jsp:include page="footer.jsp"/>