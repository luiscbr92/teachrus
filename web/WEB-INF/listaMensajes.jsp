<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
    <jsp:param name="menu" value="home"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Gente con la que has conversado</h1>
    <div class="col-md-8 col-md-offset-2">

        <ul style="list-style-type:none">


            <%
              ArrayList<String> lista = (ArrayList<String>) request.getAttribute("listaDeMensajes");
              if (lista != null)
                for (String nombre : lista) {
            %>

            <li>
                <div class="row well well-sm">
                    <div class="col-md-6">
                        <p style="font-size:20px; ">
                            <a href="/TeachRUs/verperfil/<%= nombre%>"><span class="glyphicon glyphicon-user"></span></a> <%= nombre%>
                        </p>
                    </div>

                    <div class="col-md-6">
                        <a class="btn btn-block btn-info" href="/TeachRUs/Mensaje/<%= nombre%>" role="button">
                            Ver conversación
                        </a>
                    </div>
                </div>
            </li>

            <% }%>


        </ul>
    </div>



</div><!-- contenido del col de la derecha -->

<jsp:include page="footer.jsp"/>
