<%@page import="utils.Pair"%>
<%@page import="business.Usuario"%>
<%@page import="java.util.ArrayList"%>
<jsp:include page="header.jsp">
    <jsp:param name="menu" value="ranking"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">

    <div class="row">

        <div class="col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6 text-center">
            <h3>Ranking de gente con más reputación</h3>

            <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
                <table class="table table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Reputación</strong></td>
                        </tr>

                    </thead>
                    <tbody>
                        <%
                          ArrayList<Pair<Usuario, Double>> listaRankingReputacion = (ArrayList<Pair<Usuario, Double>>) request.getAttribute("listaRankingReputacion");
                          if (listaRankingReputacion != null && listaRankingReputacion.size() > 0) {
                        %>
                        <tr class="success">
                            <td>
                                <a href="/TeachRUs/verperfil/<%= listaRankingReputacion.get(0).getLeft().getNick()%>">
                                    <%= listaRankingReputacion.get(0).getLeft().getNombre()%> <%= listaRankingReputacion.get(0).getLeft().getApellidos()%>
                                </a>
                            </td>
                            <td><%= listaRankingReputacion.get(0).getRight()%><span class="glyphicon glyphicon-heart" aria-hidden="true"></span></td>
                        </tr>
                        <%
                          for (int i = 1; i < listaRankingReputacion.size(); i++) {
                        %>
                        <tr>
                            <td>
                                <a href="/TeachRUs/verperfil/<%= listaRankingReputacion.get(i).getLeft().getNick()%>">
                                    <%= listaRankingReputacion.get(i).getLeft().getNombre()%> <%= listaRankingReputacion.get(i).getLeft().getApellidos()%>
                                </a>
                            </td>
                            <td><%= listaRankingReputacion.get(i).getRight()%><span class="glyphicon glyphicon-heart" aria-hidden="true"></span></td>
                        </tr>

                        <%
                            }
                          }
                        %>
                    </tbody>
                </table>
            </div>


        </div>
        <div class="col-sm-1 col-md-3"></div>   

    </div>


    <div class="row">

        <div class="col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6  text-center">
            <h3>Ranking de gente que más ayuda</h3>

            <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
                <table class="table table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td><strong>Cantidad de veces<br/> que ha ayudado</strong></td>
                        </tr>

                    </thead>
                    <tbody>
                        <%
                          ArrayList<Pair<Usuario, Integer>> listaRankingOfrecidas = (ArrayList<Pair<Usuario, Integer>>) request.getAttribute("listaRankingOfrecidas");
                          if (listaRankingOfrecidas != null && listaRankingOfrecidas.size() > 0) {
                        %>
                        <tr class="success">
                            <td>
                                <a href="/TeachRUs/verperfil/<%= listaRankingOfrecidas.get(0).getLeft().getNick()%>">
                                    <%=listaRankingOfrecidas.get(0).getLeft().getNombre()%> <%=listaRankingOfrecidas.get(0).getLeft().getApellidos()%>
                                </a>
                            </td>
                            <td><%=listaRankingOfrecidas.get(0).getRight()%></td>
                        </tr>
                        <%
                          for (int i = 1; i < listaRankingOfrecidas.size(); i++) {

                        %>
                        <tr>
                            <td>
                                <a href="/TeachRUs/verperfil/<%= listaRankingOfrecidas.get(i).getLeft().getNick()%>">
                                    <%=listaRankingOfrecidas.get(i).getLeft().getNombre()%> <%=listaRankingOfrecidas.get(i).getLeft().getApellidos()%>
                                </a>
                            </td>
                            <td><%=listaRankingOfrecidas.get(i).getRight()%></td>
                        </tr>
                        <%
                            }
                          }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-1 col-md-3"></div>   


    </div>

</div><!-- /.container -->

<jsp:include page="footer.jsp"/>
