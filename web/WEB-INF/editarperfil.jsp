
<%@page import="business.Usuario"%>
<jsp:include page="header.jsp">
    <jsp:param name="menu" value=""/>
</jsp:include>
<jsp:include page="sidebar.jsp"/>

<%
  Usuario u = (Usuario) session.getAttribute("usuario");
  String warning = (String) request.getAttribute("warning");
%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="row">
        <div class="col-md-3 col-md-offset-3">
            <span style="font-size: 120px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
        </div>
        <div class="col-md-3" >
            <div class="row">
                <h3><%=u.getNick()%></h3>
            </div><!--row  interno-->
            <div class="row">
                <span style="font-size: 40px;" class="glyphicon glyphicon-heart" aria-hidden="true"></span>
            <p style="margin-left: 10px"><%= request.getAttribute("reputacion") %></p>
            </div><!--row  interno-->
        </div>

    </div><!--row  -->

    <form action="/TeachRUs/editarperfil" method="post">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h4>Datos personales</h4>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input name="nombre" type="text" class="form-control" placeholder="<%=u.getNombre()%>"/>
                </div>
                <div class="form-group">
                    <label for="apellidos">Apellidos</label>
                    <input name="apellidos" type="text" class="form-control" placeholder="<%=u.getApellidos()%>"/>
                </div>
                <div class="form-group">
                    <label for="ciudad">Ciudad</label>
                    <input name="ciudad" type="text" class="form-control" placeholder="<%=u.getCiudad()%>"/>
                </div>

                <div class="form-group">
                    <label for="carrera">Carrera</label>
                    <input name="carrera" type="text" class="form-control" placeholder="<%=u.getCarrera()%>"/>
                </div>
                
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea name="description" class="form-control"><%=u.getDescripcion()%></textarea>
                </div>
            </div><!--col6-->
        </div><!--row-->


        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h4>Cambiar contraseña</h4>                      
                <div class="form-group">
                    <label for="password">Nueva contraseña</label>
                    <input name="password" type="password" class="form-control" placeholder="**********"/>
                </div>

                <div class="form-group">
                    <label for="passwordValidation">Confirmar contraseña</label>
                    <input name="passwordValidation" type="password" class="form-control" placeholder="**********"/>
                </div>
                <%if (warning != null) {%>
                <div class="alert alert-warning" role="alert"><%=warning%></div>
                <% }%>

                <div class="row">
                    <div class="col-md-4">
                        <button type="reset"  class="btn btn-default">Cancelar</button>
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <button type="submit"  class="btn btn-block btn-success">Guardar cambios</button>
                    </div>
                </div>
            </div>
        </div>
    </form>


</div>
<jsp:include page="footer.jsp"/>
