<%@page import="business.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/TeachRUs/assets/favicon.ico">

    <title>Teach r us</title>

    <!-- Bootstrap core CSS -->
    <link href="/TeachRUs/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="/TeachRUs/css/style.css" type="text/css" rel="stylesheet">
    <link href="/TeachRUs/css/dashboard.css" type="text/css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="padding-top: 70px;">

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/TeachRUs/home">
            <img style="height: 25px; margin-right: 50px" alt="Brand" src="/TeachRUs/assets/teach2.png">
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">

            <li <% if (request.getParameter("menu").equals("home")) { %> class="active"<% } %>><a href="/TeachRUs/home">Inicio <span class="sr-only">(current)</span></a></li>
            <li <% if (request.getParameter("menu").equals("necesitas")) { %> class="active"<% } %>><a href="/TeachRUs/necesitas">Necesitas</a></li>
            <li <% if (request.getParameter("menu").equals("historial")) { %> class="active"<% } %>><a href="/TeachRUs/Historial">Historial</a></li>

          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li <% if (request.getParameter("menu").equals("ranking")) { %> class="active"<% } %>><a href="/TeachRUs/ranking">Ranking</a></li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header"><h4><%= ((Usuario)session.getAttribute("usuario")).getNombre()%> <%= ((Usuario)session.getAttribute("usuario")).getApellidos() %></h4></li>
                <li role="separator" class="divider"></li>
                <li><a href="/TeachRUs/listamensajes">Mensajes</a></li>
                <li><a href="/TeachRUs/editarperfil">Editar perfil</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/TeachRUs/ayuda">Ayuda</a></li>
                <li><a href="/TeachRUs/logout">Salir</a></li>
              </ul>
            </li>
          </ul>

          </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>


    <div class="container-fluid">
      <div class="row">



