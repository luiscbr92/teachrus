<jsp:include page="header.jsp">
  <jsp:param name="menu" value=""/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Ayuda</h1>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                ¿Cómo escribo una petición de ayuda?
                </a>
                </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    Para escribir una petición de ayuda, tienes que iniciar sesión, hacer clic en "Necesitas" en la barra superior de la página y despues un último clic en el botón azul "Nueva petición". Se te mostrará un formulario en el que debes introducir la asignatura, una breve descripción sobre aquello en lo que necesitas ayuda y el número de créditos que darás a quien te ayude.
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      ¿Cómo cambio datos de mi perfil?
                    </a>
                  </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                  <div class="panel-body">
                    Para cambiar los datos de tu perfil, tienes que iniciar sesión. Después hay que hacer clic en el botón superior de la derecha para abrir el desplegable, y en dicho desplegable, hacer clic en "Editar perfil".
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      ¿Cómo contactar con los desarrolladores?
                    </a>
                  </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                  <div class="panel-body">
                    Esta página ha sido realizada por Rafael Sillero Navajas (autor de la idea original) (rafael.silnav[at]gmail.com), Luis Alberto Centeno Bragado (luiscbr92[at]gmail.com), Silvia Arias Herguedas (silviarhe95[at]gmail.com) y Anastasios Gelastopoulos (tasosgelas[at]gmail.com).
                  </div>
                </div>
              </div>
            </div>


        </div><!-- contenido del col de la derecha -->
      </div>
    </div><!-- /.container -->

<jsp:include page="footer.jsp"/>
