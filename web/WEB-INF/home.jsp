<%@page import="java.util.ArrayList"%>
<%@page import="business.Peticion"%>
<%@page import="business.Usuario"%>

<jsp:include page="header.jsp">
  <jsp:param name="menu" value="home"/>
</jsp:include>

<jsp:include page="sidebar.jsp"/>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <h1 class="page-header">Est�n buscando...</h1>

  <%
    ArrayList<Peticion> listaPeticiones = (ArrayList<Peticion>) request.getAttribute("listaPeticiones");
    for (Peticion p : listaPeticiones) {

  %>

  <div class="panel panel-default text-center">
    <div class="panel-body">
      <div class="col-md-1">
        <a href="/TeachRUs/verperfil/<%= p.getSolicitante().getNick()%>">
          <span style="font-size: 60px;" class="glyphicon glyphicon-user" aria-hidden="true"></span>
          <p class="summary"><%= p.getSolicitante().getNombre()%> <%= p.getSolicitante().getApellidos()%></p>
        </a>
      </div>
      <div class="col-md-9">
        <p class="summary"><%= p.getInfo()%></p>
      </div>
      <div class="col-md-1">
        <span style="font-size: 20px;" class="glyphicon glyphicon-bitcoin" aria-hidden="true"></span><h3><%= p.getCoste()%></h3>
      </div>
      <div class="col-md-1">
        <a class="btn btn-lg btn-info" href="/TeachRUs/detalle/<%= p.getId()%>" role="button">
          Ver
        </a>            
      </div>
    </div>
  </div>

  <%

    }

  %>


</div><!-- contenido del col de la derecha -->

<jsp:include page="footer.jsp"/>