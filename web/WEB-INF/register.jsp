<jsp:include page="headerl.jsp"/>
<div id="register_box" class="container">
 <% String warning = (String) request.getAttribute("warning"); if(warning==null) warning=""; %>
  <form action="/TeachRUs/register" method="post" class="form-signin">
    <h2 class="form-signin-heading">Registrarse</h2>

    <input type="text" name="nick" class="form-control" placeholder="Usuario" maxlength="20" required autofocus />
    <input type="text" name="nombre" class="form-control" placeholder="Nombre" maxlength="20" required />
    <input type="text" name="apellidos" class="form-control" placeholder="Apellidos" maxlength="30" required />
    <input type="text" name="ciudad" class="form-control" placeholder="Ciudad" maxlength="20" required />
    <input type="text" name="carrera" class="form-control" placeholder="Carrera" maxlength="50" required />
    
    <input type="email" name="email" class="form-control" placeholder="Correo Electr�nico" maxlength="50" required />
    <input type="password" name="pass" class="form-control" placeholder="Contrase�a" required />
    <input type="password" name="passVerification" class="form-control" placeholder="Verificar Contrase�a" required />
    <textarea class="form-control" name="descripcion" placeholder="Escribe una breve descripcion sobre ti" maxlength="140"></textarea>
    <label><%=warning%></label>
    <label>Te llegar� un correo con enlace de confirmaci�n de tu cuenta. Aseg�rate de mirarlo.</label>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
  </form>
    
    <a class="btn btn-lg btn-success btn-block" href="/TeachRUs/">Volver a inicio de sesi�n</a>

</div> <!-- /container -->

<jsp:include page="footerl.jsp"/>