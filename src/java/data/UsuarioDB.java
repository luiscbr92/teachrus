package data;

import java.sql.*;
import business.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import utils.Pair;

public class UsuarioDB {

  public static int inserta(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "INSERT INTO USUARIO (nick, nombre, apellidos, ciudad, carrera, passwd, email, creditos, descripcion, hora) VALUES (?,?,?,?,?,?,?,?,?,?)";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      ps.setString(2, u.getNombre());
      ps.setString(3, u.getApellidos());
      ps.setString(4, u.getCiudad());
      ps.setString(5, u.getCarrera());
      ps.setString(6, u.getPassword());
      ps.setString(7, u.getEmail());
      ps.setString(8, u.getCreditos().toString());
      ps.setString(9, u.getDescripcion());
      ps.setString(10, u.getFechaRegistro().format(miFormato));
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static int actualiza(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "UPDATE USUARIO SET nombre=?, apellidos=?, ciudad=?, carrera=?, passwd=?, email=?, creditos=?, descripcion=?, hora=? WHERE nick=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNombre());
      ps.setString(2, u.getApellidos());
      ps.setString(3, u.getCiudad());
      ps.setString(4, u.getCarrera());
      ps.setString(5, u.getPassword());
      ps.setString(6, u.getEmail());
      ps.setInt(7, u.getCreditos());
      ps.setString(8, u.getDescripcion());
      ps.setString(9, u.getFechaRegistro().format(miFormato));
      ps.setString(10, u.getNick());
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static Usuario selecciona(String nick) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM USUARIO WHERE nick=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, nick);
      rs = ps.executeQuery();
      Usuario u = null;
      if (rs.next()) {
        u = new Usuario(rs.getString("nick"), rs.getString("nombre"), rs.getString("apellidos"), rs.getString("ciudad"), rs.getString("carrera"), rs.getString("email"), rs.getString("passwd"), rs.getString("descripcion"), rs.getInt("creditos"), LocalDateTime.parse(rs.getString("hora"), miFormato));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return u;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Usuario selLogin(String nick, String pass) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM USUARIO WHERE nick=? AND passwd=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, nick);
      ps.setString(2, pass);
      rs = ps.executeQuery();
      Usuario u = null;
      if (rs.next()) {
        u = new Usuario(rs.getString("nick"), rs.getString("nombre"), rs.getString("apellidos"), rs.getString("ciudad"), rs.getString("carrera"), rs.getString("email"), rs.getString("passwd"), rs.getString("descripcion"), rs.getInt("creditos"), LocalDateTime.parse(rs.getString("hora"), miFormato));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return u;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Integer numeroAyudasOfrecidas(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT COUNT(*) AS value FROM PETICION P, RESPUESTA R WHERE P.id_ptc=R.id_ptc AND P.id_rsp_ac=R.id_rsp AND R.autor=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      Integer value = null;
      if (rs.next()) {
        value = rs.getInt("value");
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return value;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Integer numeroAyudasRecibidas(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT COUNT(*) AS value FROM PETICION WHERE id_rsp_ac IS NOT NULL AND solicitante=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      Integer value = null;
      if (rs.next()) {
        value = rs.getInt("value");
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return value;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Integer numeroDiasRegistrado(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT DATEDIFF(NOW(), hora) as dias FROM USUARIO WHERE nick=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      Integer dias = null;
      if (rs.next()) {
        dias = rs.getInt("dias");
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return dias;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Integer numeroPuntuacionesRecibidas(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT COUNT(*) AS value FROM PETICION P, RESPUESTA R WHERE P.id_rsp_ac=R.id_rsp AND P.puntuacion IS NOT NULL AND R.autor=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      Integer value = null;
      if (rs.next()) {
        value = rs.getInt("value");
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return value;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<Pair<Usuario, Double>> usuariosPorReputacion() {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT U.*, c.puntos/c.colabs AS reputacion "
            + "FROM USUARIO U, (SELECT COUNT(*) AS colabs, R.autor, SUM(puntuacion) as puntos "
            + "                FROM PETICION P, RESPUESTA R "
            + "                WHERE P.id_rsp_ac=R.id_rsp "
            + "                GROUP BY R.autor) c "
            + "WHERE c.colabs > 0 AND U.nick = c.autor "
            + "ORDER BY reputacion DESC";
    try {
      ps = connection.prepareStatement(query);
      rs = ps.executeQuery();
      ArrayList<Pair<Usuario, Double>> listaUsuarios = new ArrayList<Pair<Usuario, Double>>();
      while (rs.next()) {
        listaUsuarios.add(new Pair(new Usuario(rs.getString("nick"), rs.getString("nombre"),
                rs.getString("apellidos"), rs.getString("ciudad"), rs.getString("carrera"), rs.getString("email"),
                rs.getString("passwd"), rs.getString("descripcion"), rs.getInt("creditos"), LocalDateTime.parse(rs.getString("hora"), miFormato)),
                rs.getDouble("reputacion")));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaUsuarios;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<Pair<Usuario, Integer>> usuariosPorAyudasOfrecidas() {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT U.*, R.autor, COUNT(R.id_rsp) AS ayudas "
            + "FROM USUARIO U, PETICION P, RESPUESTA R "
            + "WHERE U.nick=R.autor AND P.id_rsp_ac=R.id_rsp "
            + "GROUP BY R.autor "
            + "ORDER BY ayudas DESC";
    try {
      ps = connection.prepareStatement(query);
      rs = ps.executeQuery();
      ArrayList<Pair<Usuario, Integer>> listaUsuarios = new ArrayList<Pair<Usuario, Integer>>();
      while (rs.next()) {
        listaUsuarios.add(new Pair(new Usuario(rs.getString("nick"), rs.getString("nombre"),
                rs.getString("apellidos"), rs.getString("ciudad"), rs.getString("carrera"), rs.getString("email"),
                rs.getString("passwd"), rs.getString("descripcion"), rs.getInt("creditos"), LocalDateTime.parse(rs.getString("hora"), miFormato)),
                rs.getInt("ayudas")));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaUsuarios;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static Double getReputacion(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT SUM(P.puntuacion)/COUNT(*) AS value "
            + "FROM PETICION P, RESPUESTA R "
            + "WHERE P.id_rsp_ac=R.id_rsp AND R.autor=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      Double value = null;
      if (rs.next()) {
        value = rs.getDouble("value");
        if (value == null) {
          value = 0.0;
        }
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return value;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
