package data;

import java.sql.*;
import business.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RespuestaDB {

  public static int inserta(Respuesta r) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "INSERT INTO RESPUESTA (id_rsp, autor, texto, id_ptc, hora) VALUES (?,?,?,?,?)";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, r.getId());
      ps.setString(2, r.getAutor().getNick());
      ps.setString(3, r.getTexto());
      ps.setString(4, r.getPeticion().getId());
      ps.setString(5, r.getFechaPublicacion().format(miFormato));
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static int actualiza(Respuesta r) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "UPDATE RESPUESTA SET autor=?, texto=?, id_ptc=?, hora=?) WHERE id_rsp=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, r.getAutor().getNick());
      ps.setString(2, r.getTexto());
      ps.setString(3, r.getPeticion().getId());
      ps.setString(4, r.getFechaPublicacion().format(miFormato));
      ps.setString(5, r.getId());
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static Respuesta selecciona(String id) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM RESPUESTA WHERE id_rsp=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, id);
      rs = ps.executeQuery();
      Respuesta r = null;
      if (rs.next()) {
        r = new Respuesta(rs.getString("id_rsp"), UsuarioDB.selecciona(rs.getString("autor")), rs.getString("texto"), PeticionDB.selecciona(rs.getString("id_ptc")), LocalDateTime.parse(rs.getString("hora"), miFormato));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return r;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public static Respuesta instanciaRespuestaAceptada(Peticion p){
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT R.* FROM RESPUESTA R, PETICION P WHERE P.id_rsp_ac=R.id_rsp AND P.id_ptc=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, p.getId());
      rs = ps.executeQuery();
      Respuesta r = null;
      if (rs.next()) {
        r = new Respuesta(rs.getString("id_rsp"), UsuarioDB.selecciona(rs.getString("autor")), rs.getString("texto"), p, LocalDateTime.parse(rs.getString("hora"), miFormato));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return r;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
