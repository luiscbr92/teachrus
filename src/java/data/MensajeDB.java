package data;

import java.sql.*;
import business.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class MensajeDB {

  public static int inserta(Mensaje m) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "INSERT INTO MENSAJE (id_msj, emisor, receptor, texto, leido, hora) VALUES (?,?,?,?,?,?)";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, m.getId());
      ps.setString(2, m.getEmisor().getNick());
      ps.setString(3, m.getReceptor().getNick());
      ps.setString(4, m.getTexto());
      ps.setBoolean(5, m.getLeido());
      ps.setString(6, m.getHoraEnvio().format(miFormato));
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static int actualiza(Mensaje m) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "UPDATE MENSAJE SET emisor=?, receptor=?, texto=?, leido=', hora=? WHERE id_msj=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, m.getEmisor().getNick());
      ps.setString(2, m.getReceptor().getNick());
      ps.setString(3, m.getTexto());
      ps.setBoolean(4, m.getLeido());
      ps.setString(5, m.getHoraEnvio().format(miFormato));
      ps.setString(6, m.getId());
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static Mensaje selecciona(String id) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM MENSAJE WHERE id_msj=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, id);
      rs = ps.executeQuery();
      Mensaje m = null;
      if (rs.next()) {
        m = new Mensaje(rs.getString("id_msj"), UsuarioDB.selecciona(rs.getString("emisor")), UsuarioDB.selecciona(rs.getString("receptor")), rs.getString("texto"), rs.getBoolean("leido"), LocalDateTime.parse(rs.getString("hora"), miFormato));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return m;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<Mensaje> getConversacion(Usuario u1, Usuario u2) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM MENSAJE WHERE (emisor=? AND receptor=?) OR (emisor=? AND receptor=?) ORDER BY hora DESC";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u1.getNick());
      ps.setString(2, u2.getNick());
      ps.setString(3, u2.getNick());
      ps.setString(4, u1.getNick());
      rs = ps.executeQuery();
      ArrayList<Mensaje> listaMensajes = new ArrayList<Mensaje>();
      while (rs.next()) {
        listaMensajes.add(new Mensaje(rs.getString("id_msj"), UsuarioDB.selecciona(rs.getString("emisor")), UsuarioDB.selecciona(rs.getString("receptor")), rs.getString("texto"), rs.getBoolean("leido"), LocalDateTime.parse(rs.getString("hora"), miFormato)));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaMensajes;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<String> getUsuariosConversados(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT u.nick FROM USUARIO u WHERE u.nick IN (SELECT m1.emisor FROM MENSAJE m1 WHERE m1.receptor =?) OR u.nick IN (SELECT m2.emisor FROM MENSAJE m2 WHERE m2.receptor =?) ORDER BY u.nick ASC;";
    try {
      ps = connection.prepareStatement(query);
      System.err.println(u.getNick());
      ps.setString(1, u.getNick());
      ps.setString(2, u.getNick());
      rs = ps.executeQuery();
      ArrayList<String> listaMensajes = new ArrayList<>();
      while (rs.next()) {
        listaMensajes.add(rs.getString("nick"));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaMensajes;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
