package data;

import java.sql.*;
import business.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class PeticionDB {

  public static int inserta(Peticion p) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "INSERT INTO PETICION (id_ptc, solicitante, asignatura, info, coste, hora) VALUES (?,?,?,?,?,?)";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, p.getId());
      ps.setString(2, p.getSolicitante().getNick());
      ps.setString(3, p.getAsignatura());
      ps.setString(4, p.getInfo());
      ps.setString(5, p.getCoste().toString());
      ps.setString(6, p.getFechaPublicacion().format(miFormato));
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static int actualiza(Peticion p) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;

    String query = "UPDATE PETICION SET solicitante=?, asignatura=?, info=?, coste=?, hora=?, id_rsp_ac=?, puntuacion=? WHERE id_ptc=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, p.getSolicitante().getNick());
      ps.setString(2, p.getAsignatura());
      ps.setString(3, p.getInfo());
      ps.setString(4, p.getCoste().toString());
      ps.setString(5, p.getFechaPublicacion().format(miFormato));
      ps.setString(6, p.getRespuestaAceptada().getId());
      ps.setInt(7, p.getPuntuacion());
      ps.setString(8, p.getId());
      int res = ps.executeUpdate();
      ps.close();
      pool.freeConnection(connection);
      return res;
    } catch (SQLException e) {
      e.printStackTrace();
      return 0;
    }
  }

  public static Peticion selecciona(String id) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM PETICION WHERE id_ptc=?";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, id);
      rs = ps.executeQuery();
      Peticion p = null;
      if (rs.next()) {
        p = new Peticion(rs.getString("id_ptc"), UsuarioDB.selecciona(rs.getString("solicitante")), rs.getString("info"), rs.getString("asignatura"), rs.getInt("puntuacion"), rs.getInt("coste"), null, LocalDateTime.parse(rs.getString("hora"), miFormato));
        p.setRespuestaAceptada(RespuestaDB.instanciaRespuestaAceptada(p));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return p;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<Peticion> selNoPropiasSinConfirmar(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM PETICION WHERE solicitante!=? and id_rsp_ac IS NULL";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      ArrayList<Peticion> listaPeticiones = new ArrayList<Peticion>();
      while (rs.next()) {
        listaPeticiones.add(new Peticion(rs.getString("id_ptc"), UsuarioDB.selecciona(rs.getString("solicitante")), rs.getString("info"), rs.getString("asignatura"), null, rs.getInt("coste"), null, LocalDateTime.parse(rs.getString("hora"), miFormato)));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaPeticiones;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public static ArrayList<Peticion> selPropiasSinConfirmar(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM PETICION WHERE solicitante=? and id_rsp_ac IS NULL";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      ArrayList<Peticion> listaPeticiones = new ArrayList<Peticion>();
      while (rs.next()) {
        listaPeticiones.add(new Peticion(rs.getString("id_ptc"), UsuarioDB.selecciona(rs.getString("solicitante")), rs.getString("info"), rs.getString("asignatura"), rs.getInt("puntuacion"), rs.getInt("coste"), null, LocalDateTime.parse(rs.getString("hora"), miFormato)));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaPeticiones;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public static ArrayList<Peticion> selPropiasConfirmadas(Usuario u) {
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM PETICION WHERE solicitante=? and id_rsp_ac IS NOT NULL";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, u.getNick());
      rs = ps.executeQuery();
      ArrayList<Peticion> listaPeticiones = new ArrayList<Peticion>();
      while (rs.next()) {
        Peticion p = new Peticion(rs.getString("id_ptc"), UsuarioDB.selecciona(rs.getString("solicitante")), rs.getString("info"), rs.getString("asignatura"), rs.getInt("puntuacion"), rs.getInt("coste"), null, LocalDateTime.parse(rs.getString("hora"), miFormato));
        p.setRespuestaAceptada(RespuestaDB.instanciaRespuestaAceptada(p));
        listaPeticiones.add(p);
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaPeticiones;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
  
  public static ArrayList<Respuesta> getRespuestas(Peticion p){
    ConnectionPool pool = ConnectionPool.getInstance();
    Connection connection = pool.getConnection();

    DateTimeFormatter miFormato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");

    PreparedStatement ps = null;
    ResultSet rs = null;

    String query = "SELECT * FROM RESPUESTA WHERE id_ptc=? ORDER BY hora DESC";
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, p.getId());
      rs = ps.executeQuery();
      ArrayList<Respuesta> listaRespuestas = new ArrayList<>();
      while (rs.next()) {
        listaRespuestas.add(new Respuesta(rs.getString("id_rsp"), UsuarioDB.selecciona(rs.getString("autor")), rs.getString("texto"), p, LocalDateTime.parse(rs.getString("hora"), miFormato)));
      }
      rs.close();
      ps.close();
      pool.freeConnection(connection);
      return listaRespuestas;
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }
}
