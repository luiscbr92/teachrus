package business;

import java.time.LocalDateTime;

public class Usuario {

    private String nick;
    private String nombre;
    private String apellidos;
    private String ciudad;
    private String carrera;
    private String email;
    private String password;
    private String descripcion;
    private Integer creditos;
    private LocalDateTime fechaRegistro;

    public Usuario(String nick, String nombre, String apellidos, String ciudad,
            String carrera, String email, String password, String descripcion, Integer creditos,
            LocalDateTime fechaRegistro) {
        this.nick = nick;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.ciudad = ciudad;
        this.carrera = carrera;
        this.email = email;
        this.password = password;
        this.descripcion = descripcion;
        this.creditos = creditos;
        this.fechaRegistro = fechaRegistro;
    }

    public Usuario(String nick, String nombre, String apellidos, String ciudad,
            String carrera, String email, String password, String descripcion) {
        this(nick, nombre, apellidos, ciudad, carrera, email, password, descripcion, 1500, LocalDateTime.now());
    }

    public String getNick() {
        return nick;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getCarrera() {
        return carrera;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    public boolean esIgual(Usuario otro){
      return getNick().equals(otro.getNick());
    }

}
