package business;

import java.time.LocalDateTime;
import java.util.UUID;

public class Mensaje {

  private String id;
  private Usuario emisor;
  private Usuario receptor;
  private String texto;
  private Boolean leido;
  private LocalDateTime horaEnvio;

  public Mensaje(String id, Usuario emisor, Usuario receptor, String texto, Boolean leido, LocalDateTime horaEnvio) {
    this.id = id;
    this.emisor = emisor;
    this.receptor = receptor;
    this.texto = texto;
    this.leido = leido;
    this.horaEnvio = horaEnvio;
  }

  public Mensaje(Usuario emisor, Usuario receptor, String texto) {
    this(UUID.randomUUID().toString(), emisor, receptor, texto, Boolean.FALSE, LocalDateTime.now());
  }

  public String getId() {
    return id;
  }

  public Usuario getEmisor() {
    return emisor;
  }

  public Usuario getReceptor() {
    return receptor;
  }

  public String getTexto() {
    return texto;
  }

  public Boolean getLeido() {
    return leido;
  }

  public LocalDateTime getHoraEnvio() {
    return horaEnvio;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setEmisor(Usuario emisor) {
    this.emisor = emisor;
  }

  public void setReceptor(Usuario receptor) {
    this.receptor = receptor;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }

  public void setLeido(Boolean leido) {
    this.leido = leido;
  }

  public void setHoraEnvio(LocalDateTime horaEnvio) {
    this.horaEnvio = horaEnvio;
  }

}
