package business;

import java.time.LocalDateTime;
import java.util.UUID;

public class Peticion {

  private String id;
  private Usuario solicitante;
  private String info;
  private String asignatura;
  private Integer puntuacion;
  private Integer coste;
  private Respuesta respuestaAceptada;
  private LocalDateTime fechaPublicacion;

  public Peticion(String id, Usuario solicitante, String info, String asignatura, Integer puntuacion, Integer coste, Respuesta respuestaAceptada, LocalDateTime fechaPublicacion) {
    this.id = id;
    this.solicitante = solicitante;
    this.info = info;
    this.asignatura = asignatura;
    this.puntuacion = puntuacion;
    this.coste = coste;
    this.respuestaAceptada = respuestaAceptada;
    this.fechaPublicacion = fechaPublicacion;
  }

  public Peticion(Usuario solicitante, String info, String asignatura, Integer coste) {
    this(UUID.randomUUID().toString(), solicitante, info, asignatura, null, coste, null, LocalDateTime.now());
  }

  public String getId() {
    return id;
  }

  public Usuario getSolicitante() {
    return solicitante;
  }

  public String getInfo() {
    return info;
  }

  public String getAsignatura() {
    return asignatura;
  }

  public Integer getPuntuacion() {
    return puntuacion;
  }

  public Integer getCoste() {
    return coste;
  }

  public Respuesta getRespuestaAceptada() {
    return respuestaAceptada;
  }

  public LocalDateTime getFechaPublicacion() {
    return fechaPublicacion;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setSolicitante(Usuario solicitante) {
    this.solicitante = solicitante;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public void setAsignatura(String asignatura) {
    this.asignatura = asignatura;
  }

  public void setPuntuacion(Integer puntuacion) {
    this.puntuacion = puntuacion;
  }

  public void setCoste(Integer coste) {
    this.coste = coste;
  }

  public void setRespuestaAceptada(Respuesta respuestaAceptada) {
    this.respuestaAceptada = respuestaAceptada;
  }

  public void setFechaPublicacion(LocalDateTime fechaPublicacion) {
    this.fechaPublicacion = fechaPublicacion;
  }

}
