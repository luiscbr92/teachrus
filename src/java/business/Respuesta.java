package business;

import java.time.LocalDateTime;
import java.util.UUID;

public class Respuesta {

  private String id;
  private Usuario autor;
  private String texto;
  private Peticion peticion;
  private LocalDateTime fechaPublicacion;

  public Respuesta(String id, Usuario autor, String texto, Peticion peticion, LocalDateTime fechaPublicacion) {
    this.id = id;
    this.autor = autor;
    this.texto = texto;
    this.peticion = peticion;
    this.fechaPublicacion = fechaPublicacion;
  }

  public Respuesta(Usuario autor, String texto, Peticion peticion) {
    this(UUID.randomUUID().toString(), autor, texto, peticion, LocalDateTime.now());
  }

  public String getId() {
    return id;
  }

  public Usuario getAutor() {
    return autor;
  }

  public String getTexto() {
    return texto;
  }

  public Peticion getPeticion() {
    return peticion;
  }

  public LocalDateTime getFechaPublicacion() {
    return fechaPublicacion;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setAutor(Usuario autor) {
    this.autor = autor;
  }

  public void setTexto(String texto) {
    this.texto = texto;
  }

  public void setPeticion(Peticion peticion) {
    this.peticion = peticion;
  }

  public void setFechaPublicacion(LocalDateTime fechaPublicacion) {
    this.fechaPublicacion = fechaPublicacion;
  }

  public Boolean esIgual(Respuesta otra){
    return getId().equals(otra.getId());
  }
}
