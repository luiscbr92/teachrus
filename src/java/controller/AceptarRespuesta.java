/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import business.*;
import data.PeticionDB;
import data.RespuestaDB;
import data.UsuarioDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.Pair;

/**
 *
 * @author luiscbr92
 */
public class AceptarRespuesta extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {

      String peticion = request.getParameter("peticion");
      String respuesta = request.getParameter("respuesta");

      Peticion p = PeticionDB.selecciona(peticion);
      Respuesta r = RespuestaDB.selecciona(respuesta);
      p.setRespuestaAceptada(r);
      p.getSolicitante().setCreditos(p.getSolicitante().getCreditos() - p.getCoste());
      p.getRespuestaAceptada().getAutor().setCreditos(p.getRespuestaAceptada().getAutor().getCreditos() + p.getCoste());
      UsuarioDB.actualiza(p.getSolicitante());
      UsuarioDB.actualiza(p.getRespuestaAceptada().getAutor());
      PeticionDB.actualiza(p);
      session.setAttribute("usuario", p.getSolicitante());
      response.sendRedirect("/TeachRUs/Historial");
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.sendRedirect("/TeachRUs/");
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
