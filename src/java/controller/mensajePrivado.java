/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import business.Usuario;
import business.Mensaje;
import data.MensajeDB;
import data.UsuarioDB;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rafsill
 */
public class mensajePrivado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);

        if (session == null) {
            response.sendRedirect("/TeachRUs/");

        } else {
            Usuario u = (Usuario) session.getAttribute("usuario");

            if (u == null) {
                response.sendRedirect("/TeachRUs/");
            } else {

                System.out.println(request);
                String nombreU2 = request.getPathInfo().substring(1);
                Usuario u2 = UsuarioDB.selecciona(nombreU2);
                ArrayList<Mensaje> conversacionMensajes = MensajeDB.getConversacion(u, u2);
                if (conversacionMensajes != null) {
                    request.setAttribute("conversacionMensajes", conversacionMensajes);
                    
                }
                getServletContext().getRequestDispatcher("/WEB-INF/mensajePrivado.jsp").forward(request, response);
                for(int i = 0; i<conversacionMensajes.size();i++){
                    conversacionMensajes.get(i).setLeido(Boolean.TRUE);
                }
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
       

        if (session == null) {
            response.sendRedirect("/TeachRUs/");

        } else {
            Usuario emisor = (Usuario) session.getAttribute("usuario");
            String mens=request.getParameter("mens");
            String nickReceptor=request.getPathInfo().substring(1);
            Usuario receptor= UsuarioDB.selecciona(nickReceptor);
            
            if(mens.equals("")){
                request.setAttribute("error","El mensaje está vacío");
            }
            else{
                Mensaje mensajeNuevo =  new Mensaje(emisor,receptor,mens);
                MensajeDB.inserta(mensajeNuevo);
            }
            getServletContext().getRequestDispatcher("/WEB-INF/mensajePrivado.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
