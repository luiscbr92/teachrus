/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import business.Usuario;
import data.UsuarioDB;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tasoszg
 */
public class editarperfil extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {
      request.setAttribute("reputacion", UsuarioDB.getReputacion(u));
      getServletContext().getRequestDispatcher("/WEB-INF/editarperfil.jsp").forward(request, response);
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {
      String warning = null;

      String nombre = (String) request.getParameter("nombre");
      String apellidos = (String) request.getParameter("apellidos");
      String ciudad = (String) request.getParameter("ciudad");
      String carrera = (String) request.getParameter("carrera");
      String password = (String) request.getParameter("password");
      String passwordValidation = (String) request.getParameter("passwordValidation");
      String descripcion = (String) request.getParameter("descripcion");
      boolean hacerConsulta = true;
      if (nombre != null && !nombre.equals("")) {
        u.setNombre(nombre);
      }
      if (apellidos != null && !apellidos.equals("")) {
        u.setApellidos(apellidos);
      }
      if (ciudad != null && !ciudad.equals("")) {
        u.setCiudad(ciudad);
      }
      if (carrera != null && !carrera.equals("")) {
        u.setCarrera(carrera);
      }
      if (descripcion != null && !descripcion.equals("")) {
        u.setDescripcion(descripcion);
      }
      if (password != null && !password.equals("")) {
        if (passwordValidation != null && password.equals(passwordValidation)) {
          u.setPassword(password);
        } else {
          warning = "Las contraseñas no son iguales";
          hacerConsulta = false;
        }
      }
      if((password == null || password.equals("")) && passwordValidation != null){
        warning = "Las contraseñas no son iguales";
        hacerConsulta = false;
      }

      if (hacerConsulta) {
        UsuarioDB.actualiza(u);
      }
      request.setAttribute("warning", warning);
      getServletContext().getRequestDispatcher("/WEB-INF/editarperfil.jsp").forward(request, response);
    }
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
