package controller;

import business.Usuario;
import data.UsuarioDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.*;

public class index extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    if (session == null) {
      getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    } else {
      Usuario usuario = (Usuario) session.getAttribute("usuario");
      if (usuario == null) {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
      } else {
        Usuario u = UsuarioDB.selLogin(usuario.getNick(), usuario.getPassword());
        if (u == null) {
          getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        } else {
          response.sendRedirect("home");
        }
      }
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    String username = request.getParameter("inputUsername");
    String password = request.getParameter("inputPassword");

    if (username == null || password == null) {
      request.setAttribute("error", "Complete todos los campos.");
      getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    } else {
      request.setAttribute("error", null);
      Usuario u = UsuarioDB.selLogin(username, password);

      if (u == null) {
        request.setAttribute("error", "Los datos de acceso son incorrectos.");
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
      } else {
        HttpSession session = request.getSession();
        session.setAttribute("usuario", u);
        response.sendRedirect("home");
      }
    }

  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }

}
