package controller;

import business.*;
import data.PeticionDB;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class necesitas extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {
      ArrayList<Peticion> listaPeticiones = PeticionDB.selPropiasSinConfirmar(u);
      ArrayList<ArrayList<Respuesta>> todasRespuestas = new ArrayList<ArrayList<Respuesta>>();
      
      for(int i = 0; i < listaPeticiones.size(); i++)
        todasRespuestas.add(PeticionDB.getRespuestas(listaPeticiones.get(i)));

      if (listaPeticiones != null) {
        request.setAttribute("listaPeticiones", listaPeticiones);
        request.setAttribute("todasRespuestas", todasRespuestas);
      }
      getServletContext().getRequestDispatcher("/WEB-INF/necesitas.jsp").forward(request, response);
    }

  }

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }

}
