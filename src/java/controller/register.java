package controller;

import business.Usuario;
import data.UsuarioDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class register extends HttpServlet {

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
    HttpSession session = request.getSession(false);
    
    if (session == null) {
      getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
    } else {
      Usuario usuario = (Usuario) session.getAttribute("usuario");
      if (usuario == null) {
        getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
      } else {
        response.sendRedirect("/TeachRUs/");
      }
      
    }
    
  }
  
  void registrarNuevoUsuario(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
    String nick = request.getParameter("nick");
    String email = request.getParameter("email");
    String nombre = request.getParameter("nombre");
    String apellidos = request.getParameter("apellidos");
    String ciudad = request.getParameter("ciudad");
    String carrera = request.getParameter("carrera");
    String pass = request.getParameter("pass");
    String passVerification = request.getParameter("passVerification");
    String descripcion = request.getParameter("descripcion");
    
    if (nick == null || email == null || nombre == null || apellidos == null || ciudad == null || carrera == null || pass == null || passVerification == null) {
      request.setAttribute("warning", "Completa todos los campos.");
      getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
    } else if (pass.equals(passVerification)) {
      if (UsuarioDB.selecciona(nick) == null) {
        Usuario usuario = new Usuario(nick, nombre, apellidos, ciudad, carrera, email, pass, descripcion);
        UsuarioDB.inserta(usuario);
        response.sendRedirect("/TeachRUs/");
      } else {
        request.setAttribute("warning", "Usuario ya existe!");
        getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
      }
    } else {
      request.setAttribute("warning", "Las contraseñas no son iguales!");
      getServletContext().getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
    }
    
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
    HttpSession session = request.getSession(false);
    
    if (session == null) {
      registrarNuevoUsuario(request, response);
    } else {
      Usuario usuario = (Usuario) session.getAttribute("usuario");
      if (usuario == null) {
        registrarNuevoUsuario(request, response);
      } else {
        response.sendRedirect("/TeachRUs/");
      }
    }
    
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }
  
}
