package controller;

import business.*;
import data.*;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class detalle extends HttpServlet {

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);

    if (session == null) {
      response.sendRedirect("/TeachRUs/");
    } else {
      Usuario u = (Usuario) session.getAttribute("usuario");
      if (u == null) {
        response.sendRedirect("/TeachRUs/");
      } else {
        String id = request.getPathInfo().substring(1);
        Peticion p = (Peticion) PeticionDB.selecciona(id);
        ArrayList<Respuesta> listaRespuestas = (ArrayList<Respuesta>) PeticionDB.getRespuestas(p);
        request.setAttribute("peticion", p);
        request.setAttribute("listaRespuestas", listaRespuestas);
        getServletContext().getRequestDispatcher("/WEB-INF/detalle.jsp").forward(request, response);
      }
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {

      String textoRespuesta = request.getParameter("texto");

      if (textoRespuesta.equals("") || textoRespuesta == null) {
        request.setAttribute("error", "Complete todos los campos.");
        getServletContext().getRequestDispatcher("/WEB-INF/detalle.jsp").forward(request, response);
      } else {
        String id = request.getPathInfo().substring(1);
        Peticion p = (Peticion) PeticionDB.selecciona(id);
        Respuesta r = new Respuesta(u, textoRespuesta, p);
        RespuestaDB.inserta(r);
        response.sendRedirect("/TeachRUs/detalle/" + p.getId());
      }
    }

  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }

}
