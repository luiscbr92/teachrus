package controller;

import business.Peticion;
import business.Usuario;
import data.PeticionDB;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class nuevapeticion extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
  }

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {
      getServletContext().getRequestDispatcher("/WEB-INF/nuevapeticion.jsp").forward(request, response);
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    HttpSession session = request.getSession(false);
    Usuario u = (Usuario) session.getAttribute("usuario");

    if (u == null) {
      response.sendRedirect("/TeachRUs/");
    } else {

      String asignatura = request.getParameter("asignatura");
      String creditos = request.getParameter("creditos");
      String desc = request.getParameter("desc");

      if (asignatura.equals("") || creditos.equals("") || desc.equals("")) {
        request.setAttribute("error", "Complete todos los campos.");
        getServletContext().getRequestDispatcher("/WEB-INF/nuevapeticion.jsp").forward(request, response);
      } else {
        Peticion p = new Peticion(u, desc, asignatura, Integer.parseInt(creditos));
        PeticionDB.inserta(p);
        response.sendRedirect("/TeachRUs/detalle/" + p.getId());
      }
    }
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }

}
